<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
	<meta charset="<?php bloginfo( 'charset' ); ?>">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no">
	<title><?php bloginfo('name'); ?> | <?php is_home() ? bloginfo('description') : wp_title(''); ?></title>
	<link rel="icon" type="image/png" href="<?php echo get_template_directory_uri(); ?>/images/favicon.png">
	<link href='https://fonts.googleapis.com/css?family=Lato:300,400,700,400italic' rel='stylesheet' type='text/css'>
	<!--[if lt IE 9]>
		<script src="<?php //echo get_stylesheet_directory_uri(); ?>/js/libs/html5shiv/html5shiv.js"></script>
		<link rel="stylesheet"  href="<?php //echo get_stylesheet_directory_uri(); ?>/css/style-ie8.css" type="text/css" media="screen" />
	<![endif]-->
	
	<?php wp_head(); ?>
	
</head>
<body <?php body_class(); ?>>
<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','//www.google-analytics.com/analytics.js','ga');

  ga('create', 'UA-50151155-1', 'david-ledoux.fr');
  ga('send', 'pageview');

</script>
<div class="wrapper cf">
	
	<header class="header--main">

		<div class="grid-row">

			<div class="grid-col-3">
		
				<h1 class="site-title"><a href="<?php echo get_site_url(); ?>"><?php bloginfo('name'); ?> <span><?php bloginfo('description'); ?></span></a></h1>

			</div>
			
			<div class="grid-col-9">
				<nav class="nav--main">
					<ul>
						<?php
							$mainMenu = array(
								'menu' => '',
								'container' => '',
								'container_class' => '',
								'container_id' => '',
								'menu_class' => '',
								'menu_id' => '',
								'echo' => true,
								'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
								'depth' => 0 );
							
							wp_nav_menu($mainMenu)
						?>
					</ul>
				</nav>
			</div>
			
		</div>
	</header>