<?php
	/**
	Template Name: Blog
	**/
	get_header();
	
	$paged = (get_query_var('paged')) ? get_query_var('paged') : 1;
	
	$argsBlogPosts = array (
		'post_type'              => 'post',
		'pagination'             => true,
		'paged'                  => $paged,
		'posts_per_page'         => '10',
		'order'                  => 'DESC',
		'orderby'                => 'date',
	);
	$queryBlogPosts = new WP_Query( $argsBlogPosts );
	
?>
<?php if( $queryBlogPosts->have_posts() ) : ?>
<div class="wrapper-archives">
	<?php while( $queryBlogPosts->have_posts() ) : $queryBlogPosts->the_post(); ?>
	<div class="post">
		<?php if( has_post_thumbnail() ){ ?>
		<div class="post-thumbnail">
			<?php echo get_the_post_thumbnail( $post->ID, 'thumb-blog' ); ?>
		</div>
		<?php } ?>
		<div class="content-wrapper">
			<div class="post-content">
				<?php the_content(); ?>
				<p class="post-date"><?php echo get_the_date('d/m/Y'); ?></p>
			</div>
			<aside>
				<?php get_template_part('tpl/sharers'); ?>
			</aside>
		</div>
		<span class="clearer"></span>
	</div>
	<?php endwhile; ?>
	<div class="navigation blog">
		<?php
			if( get_previous_posts_link( '', $queryBlogPosts->max_num_pages ) ){
				echo '<span class="nav-button previous">';
				previous_posts_link( '', $queryBlogPosts->max_num_pages );
				echo '</span>';
			} else {
				echo '<span class="nav-button previous hidden">';
				previous_posts_link( '', $queryBlogPosts->max_num_pages );
				echo '</span>';
			}
		?>
		<?php
			if ($paged < 10){
				$position = '0' . $paged;
			} else {
				$position = $paged;
			}
		?>
		<span class="index current"><?php echo $position; ?></span>
		<span class="separator">•/•</span>
		<?php
			$lastPage = $queryBlogPosts->max_num_pages;
			if ($lastPage < 10){
				$lastPage = '0' . $lastPage;
			}
		?>
		<span class="index last"><?php echo $lastPage; ?></span>
		<?php
			if( get_next_posts_link( '', $queryBlogPosts->max_num_pages ) ){
				echo '<span class="nav-button next">';
				next_posts_link( '', $queryBlogPosts->max_num_pages );
				echo '</span>';
			} else {
				echo '<span class="nav-button next hidden">';
				next_posts_link( '', $queryBlogPosts->max_num_pages );
				echo '</span>';
			}
		?>
	</div>
</div>
<?php endif; ?>
<?php get_footer(); ?>