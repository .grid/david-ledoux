<?php
	//global $post;
	
	$postURL = get_permalink( get_the_ID() );
	$shareURL = urlencode( $postURL );
	
	$postTitle = get_the_title( get_the_ID() ) . ' by David Ledoux';
	$postExcerpt = get_the_excerpt();
	
?>
<span class="share">Share this gallery on...</span>
<script type="text/javascript">
	var viewportWidth = Math.max(document.documentElement.clientWidth, window.innerWidth || 0);
	var viewportHeight = Math.max(document.documentElement.clientHeight, window.innerHeight || 0);
	var w = 580;
	var h = 325;
	var leftX = (viewportWidth-w)*0.5;
	var topY = (viewportHeight-h)*0.5;
</script>
<ul class="sharers">
	<li><a href="javascript: void(0)" onClick="window.open('https://www.facebook.com/sharer/sharer.php?u=<?php echo $shareURL; ?>','sharer','toolbar=0,status=0,width='+w+', height='+h+', top='+topY+', left='+leftX);">Facebook</a></li>
	<li><a href="javascript: void(0)" onClick="window.open('http://twitter.com/share?text=<?php echo $postTitle; ?>&url=<?php echo $shareURL; ?>','sharer','toolbar=0,status=0,width='+w+', height='+h+', top='+topY+', left='+leftX);" >Twitter</a></li>
</ul>