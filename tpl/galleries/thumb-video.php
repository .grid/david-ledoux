<?php
	
	// Get Gallery
	$gallery = get_field('acf_gallery_pictures', get_the_ID());

	// Get 1st item from Gallery
	$galleryFeatured = $gallery[0];

	// Get Video meta
	$videoID = $galleryFeatured['acf_gallery_picture_video'];
	$videoMeta = wp_get_attachment_metadata($videoID);

	$videoURL = wp_get_attachment_url( $videoID );
	$videoWidth = $videoMeta['width'];
	$videoHeight = $videoMeta['height'];
	//var_dump($videoID);

?>
<article class="grid--item-content">

	<div class="video--wrapper thumbnail" data-video-url="<?php echo $videoURL; ?>" data-video-width="<?php echo $videoWidth; ?>" data-video-height="<?php echo $videoHeight; ?>">
		<video class="video-js" preload="auto">
			<source src="<?php echo $videoURL; ?>" type="video/mp4">
			<!--<source src="MY_VIDEO.webm" type="video/webm">-->
		</video>
	</div>

	<h2 class="grid--item-title"><?php echo get_the_title(); ?></h2>

	<svg class="grid--item-icon" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
		<rect class="bar-horizontal" fill="#FFFFFF" x="0" y="9" width="20" height="1"/>
		<rect class="bar-vertical" fill="#FFFFFF" x="0" y="9" width="20" height="1"/>
	</svg>

</article>