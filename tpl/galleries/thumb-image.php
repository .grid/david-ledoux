<article class="grid--item-content">

	<?php the_post_thumbnail('medium_large'); ?>

	<h2 class="grid--item-title"><?php echo get_the_title(); ?></h2>

	<svg class="grid--item-icon" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
		<rect class="bar-horizontal" x="0" y="9" width="20" height="1"/>
		<rect class="bar-vertical" x="0" y="9" width="20" height="1"/>
	</svg>

</article>