<?php
	$categoryImageID = get_field('category_featured_image', 'work_category_' . $photosCategory->term_id);
	
	echo wp_get_attachment_image($categoryImageID, array('9999','315'));

?>

<h2 class="home--dispatch-title"><?php echo $photosCategory->name; ?></h2>