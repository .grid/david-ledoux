<?php get_header(); ?>


<section id="gallery">

<?php if( have_posts() ) : ?>
			
	<?php while( have_posts() ) : the_post(); ?>
	
	<div class="gallery--fullscreen swiper-container">

		<div class="swiper-wrapper">
			
			<?php
			
			$galleryPics = get_field('acf_gallery_pictures');
			
			$i = 0;

			if( $galleryPics ){
			
				while( has_sub_field('acf_gallery_pictures') ){ ?>
				
				<?php

					$i++;

					if( get_sub_field('acf_gallery_picture_type') === 'video' ){ ?>
						
						<div class="swiper-slide video" data-history="<?php echo $i; ?>">
						
						<?php
							
							$mediaID = get_sub_field('acf_gallery_picture_video');
							$mediaInfo = wp_get_attachment_metadata($mediaID);
							
						?>
							<div class="video--wrapper" data-video-width="<?php echo $mediaInfo['width']; ?>" data-video-height="<?php echo $mediaInfo['height']; ?>">
								<video class="video-js" preload="auto">
									<source src="<?php echo wp_get_attachment_url( $mediaID ); ?>" type="video/mp4">
								</video>
							</div>
						
						</div>
			
					<?php } else { ?>
						
						<div class="swiper-slide" data-history="<?php echo $i; ?>">
						
						<?php
							echo wp_get_attachment_image( get_sub_field('acf_gallery_picture_image'), 'full', '', array('class'=>'') ); ?>
						
						</div>
						
					<?php } ?>
		
				<?php } ?>
				
			<?php } ?>


		</div>
		
		<?php if( count($galleryPics) > 1 ){ ?>
		<div class="swiper--nav">
		
			<span class="gallery--index">01</span>
			<button class="swiper--nav-previous">Previous</button>
			
			<span class="gallery--total">99</span>
			<button class="swiper--nav-next">Next</button>
			
		</div>
		<?php } ?>

	</div>
	
<?php endwhile; ?>

<?php endif; ?>

	<a class="btn-close" onclick="history.back()">
		<svg class="icon-close" xmlns="http://www.w3.org/2000/svg" width="20" height="16" viewBox="0 0 20 16">
			<rect class="icon-close--top" x="0" y="6.5" width="20" height="3"/>
			<rect class="icon-close--bottom" x="0" y="6.5" width="20" height="3"/>
		</svg>
		<i class="btn-label">Close</i>
		<i class="btn-label-alt">Back</i>
	</a>
	
</section>

<?php get_footer(); ?>