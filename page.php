<?php get_header(); ?>
<?php if( have_posts() ) : ?>
<?php while( have_posts() ) : the_post(); ?>

<div class="grid-row">
	<div class="grid-col-8 grid-offset-2">
		
		<div id="content">
			<?php the_content(); ?>
		</div>
		
	</div>
	
</div>

<?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>
