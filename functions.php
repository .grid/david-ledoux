<?php

/* SETUP THEME */
function theme_setup(){
	
	/* i18n */
	//load_theme_textdomain('david-ledoux', get_template_directory() . '/lang');

	/* Inc resources */
	require_once get_template_directory() . '/inc/enqueue-scripts.php';
	require_once get_template_directory() . '/inc/acf.php';

}
add_action( 'after_setup_theme', 'theme_setup' );

/* THEME SUPPORT */
add_theme_support('post-thumbnails', array( 'post', 'cpt-gallery' ));

/* POST TYPES */
function custom_gallery_post_type() {

	$labels = array(
		'name'                => _x( 'Galleries', 'Post Type General Name', 'david-ledoux' ),
		'singular_name'       => _x( 'Gallery', 'Post Type Singular Name', 'david-ledoux' ),
		'menu_name'           => __( 'Gallery', 'david-ledoux' ),
		'parent_item_colon'   => __( 'Parent Item:', 'david-ledoux' ),
		'all_items'           => __( 'All Galleries', 'david-ledoux' ),
		'view_item'           => __( 'View Gallery', 'david-ledoux' ),
		'add_new_item'        => __( 'Add New Gallery', 'david-ledoux' ),
		'add_new'             => __( 'Add New', 'david-ledoux' ),
		'edit_item'           => __( 'Edit Gallery', 'david-ledoux' ),
		'update_item'         => __( 'Update Gallery', 'david-ledoux' ),
		'search_items'        => __( 'Search Item', 'david-ledoux' ),
		'not_found'           => __( 'Not found', 'david-ledoux' ),
		'not_found_in_trash'  => __( 'Not found in Trash', 'david-ledoux' ),
	);
	$rewrite = array(
		'slug'                => 'gallery',
		'with_front'          => true,
		'pages'               => true,
		'feeds'               => true,
	);
	$args = array(
		'label'               => __( 'cpt-gallery', 'david-ledoux' ),
		'description'         => __( 'Photo Gallery', 'david-ledoux' ),
		'labels'              => $labels,
		'supports'            => array( 'title', 'editor', 'excerpt', 'thumbnail', 'custom-fields', ),
		'hierarchical'        => false,
		'public'              => true,
		'show_ui'             => true,
		'show_in_menu'        => true,
		'show_in_nav_menus'   => true,
		'show_in_admin_bar'   => true,
		'menu_position'       => 5,
		'menu_icon'           => 'dashicons-images-alt',
		'can_export'          => true,
		'has_archive'         => true,
		'exclude_from_search' => false,
		'publicly_queryable'  => true,
		'rewrite'             => $rewrite,
		'capability_type'     => 'page',
	);
	register_post_type( 'cpt-gallery', $args );

}

// Hook into the 'init' action
add_action( 'init', 'custom_gallery_post_type', 0 );


/* IMAGES SIZES */
add_image_size( 'featured-thumb-gallery', 330, 330, true );
add_image_size( 'thumb-gallery', 99999, 330, false );
add_image_size( 'thumb-gallery-landscape', 99999, 330, false );
//add_image_size( 'thumb-gallery-portrait', 330, 330, true );
add_image_size( 'fullsize-gallery', 660, 660, false );
add_image_size( 'thumb-blog', 520, 490, false );

/* IMAGES 100% QUALITY */
add_filter( 'jpeg_quality', 'tgm_image_full_quality' );
add_filter( 'wp_editor_set_quality', 'tgm_image_full_quality' );
function tgm_image_full_quality( $quality ) {
	return 100;
}

// Curl helper function
function curl_get($url) {
    $curl = curl_init($url);
    curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($curl, CURLOPT_TIMEOUT, 30);
    curl_setopt($curl, CURLOPT_FOLLOWLOCATION, 1);
    $return = curl_exec($curl);
    curl_close($curl);
    return $return;
}

// Register Navigation Menus
function custom_menus() {

	$locations = array(
		'Main menu' => __( 'This is the main navigation menu', 'david-ledoux' ),
	);
	register_nav_menus( $locations );

}
add_action( 'init', 'custom_menus' );


// Register Custom Taxonomy
function register_work_category() {

	$labels = array(
		'name'                       => _x( 'Work Categories', 'Taxonomy General Name', 'david-ledoux' ),
		'singular_name'              => _x( 'Work Category', 'Taxonomy Singular Name', 'david-ledoux' ),
		'menu_name'                  => __( 'Category', 'david-ledoux' ),
		'all_items'                  => __( 'All Items', 'david-ledoux' ),
		'parent_item'                => __( 'Parent Item', 'david-ledoux' ),
		'parent_item_colon'          => __( 'Parent Item:', 'david-ledoux' ),
		'new_item_name'              => __( 'New Item Name', 'david-ledoux' ),
		'add_new_item'               => __( 'Add New Item', 'david-ledoux' ),
		'edit_item'                  => __( 'Edit Item', 'david-ledoux' ),
		'update_item'                => __( 'Update Item', 'david-ledoux' ),
		'view_item'                  => __( 'View Item', 'david-ledoux' ),
		'separate_items_with_commas' => __( 'Separate items with commas', 'david-ledoux' ),
		'add_or_remove_items'        => __( 'Add or remove items', 'david-ledoux' ),
		'choose_from_most_used'      => __( 'Choose from the most used', 'david-ledoux' ),
		'popular_items'              => __( 'Popular Items', 'david-ledoux' ),
		'search_items'               => __( 'Search Items', 'david-ledoux' ),
		'not_found'                  => __( 'Not Found', 'david-ledoux' ),
		'no_terms'                   => __( 'No items', 'david-ledoux' ),
		'items_list'                 => __( 'Items list', 'david-ledoux' ),
		'items_list_navigation'      => __( 'Items list navigation', 'david-ledoux' ),
	);
	$args = array(
		'labels'                     => $labels,
		'hierarchical'               => true,
		'public'                     => true,
		'show_ui'                    => true,
		'show_admin_column'          => true,
		'show_in_nav_menus'          => true,
		'show_tagcloud'              => true,
	);
	register_taxonomy( 'work_category', array( 'cpt-gallery' ), $args );

}
add_action( 'init', 'register_work_category', 0 );



function work_nopaging( $query ) {
    if ( $query->is_tax('work_category') && $query->is_main_query() ) {
        $query->set( 'posts_per_page', '-1' );
    }
}
add_action( 'pre_get_posts', 'work_nopaging' );