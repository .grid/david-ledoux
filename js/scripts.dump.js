$(document).ready(function() {
	
	/***********************************
	 MAIN
	***********************************/

	var windowWidth = $(window).width();

	/** INTRO VIDEO OBJECT **/
	var video = jQuery('#intro-video')[0];

	/** INTRO **/
	jQuery('a.btn-enter').on('click', function(e){
		e.preventDefault();
		jQuery('#intro').fadeOut(300, function(){
			video.pause();
		});
	});
	
	/************************************
	 GALLERY HANDLER
	************************************/
	var GalleryHandler = {
		init: function(){
			
			var self = this;
			
			//self.setGalleryThumbs();

			var $grid = $('.grid').masonry({
				// options
				itemSelector: '.grid-item',
				//columnWidth: 100
			});
			
			$grid.imagesLoaded().progress( function() {
				$grid.masonry('layout');
			});
			if( $('.fullscreen-gallery').length ){
				self.setGalleryFullscreen();
			}

			self.galleryLength();

		},
		setGalleryThumbs: function(windowWidth){
			
			var self = this;
			
			var swiperGallery = new Swiper('.wrapper-gallery', {
				//wrapperClass: '#slider-gallery',
				slideClass: 'block',
				speed: 400,
				slidesPerView: 'auto',
				spaceBetween: 20,
				//freeModeSticky: true,
				slidesOffsetAfter: 430,
				keyboardControl: true,
				breakpoints: {
					1024: {
						slidesOffsetAfter: 0,
						freeMode: true,
						freeModeSticky: true
					}
				}
			});

			var galleryNavNext = $('.nav-button.next');
			var galleryNavPrev = $('.nav-button.previous');


			galleryNavNext.on('click', function(){
				swiperGallery.slideNext();
			});

			galleryNavPrev.on('click', function(){
				swiperGallery.slidePrev();
			});
			
			self.galleryThumbIndex(swiperGallery);

			swiperGallery.on('slideChangeStart', function(){
				self.galleryThumbIndex(swiperGallery);
			});
			
			
		},
		setGalleryFullscreen: function(slide){
			
			var self = this;

			var slides = '';
			var i = 1;

			$('.swiper-slide').each( function(){
				
				if( $(this).find('.gallery-thumb').attr('data-image-fullsize') ){
					
					var fullsizeImage = $(this).find('.gallery-thumb').attr('data-image-fullsize');
					
					slides += '<div class="swiper-slide" data-hash="fullscreen'+ i + '"><img src="' + fullsizeImage + '" /><span class="overlay"></span></div>';

				} else if( $(this).find('.vimeo_player').attr('data-video-id') ){
					
					var videoID 		= $(this).find('.vimeo_player').attr('data-video-id'),
						videoFullWidth 	= $(this).find('.vimeo_player').attr('data-video-fullwidth'),
						videoFullHeight = $(this).find('.vimeo_player').attr('data-video-fullheight'),
						media 			= '<iframe class="vimeo_player" src="//player.vimeo.com/video/'+ videoID +'?color=ffffff&byline=0&portrait=0" width="'+ videoFullWidth +'" height="'+ videoFullHeight +'" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen ></iframe>';
					
					slides += '<div class="swiper-slide video" data-hash="fullscreen'+ i + '">'+ media + '</div>';

				}

				
				i++;
			});

			$('.fullscreen-gallery .swiper-wrapper').append(slides);

			var swiperFullScreen = new Swiper('.fullscreen-gallery', {
				speed: 400,
				slidesPerView: 1,
				//autoHeight: true,
				effect: 'fade',
				fade: {
					crossFade: true
				},
				simulateTouch: false,
				keyboardControl: true,
				breakpoint: {
					1024:{
						//preloadImages: false,
						//lazyLoading: true,
					}
				}
			});

			swiperFullScreen.init();
			
			var galleryNavNext = $('.nav-fullsize.next');
			var galleryNavPrev = $('.nav-fullsize.previous');


			galleryNavNext.on('click', function(){
				swiperFullScreen.slideNext();
			});

			galleryNavPrev.on('click', function(){
				swiperFullScreen.slidePrev();
			});

			swiperFullScreen.on('slideChangeStart', function(){
				self.galleryFullsizeIndex(swiperFullScreen);
				self.galleryVideoOverlay();
			});


			$('.block.single').on('click', function(){
				$('#gallery-overlay').addClass('visible');
				var slide = $(this).index() - 1;
				swiperFullScreen.slideTo(slide);
				self.galleryFullsizeIndex(swiperFullScreen);
			});

			$('body').on('click', '.img-fullsize .swiper-slide-active .overlay', function(){
				$('#gallery-overlay').removeClass('visible');
			});

		},
		galleryThumbIndex: function(swiper){

			var $indexThumb	= swiper.activeIndex;

			if( !$('.block.gallery.intro').length ){
				$indexThumb = $indexThumb + 1;
			}

			if( $indexThumb < 10 ){
				$indexThumb = '0' + $indexThumb;
			}

			$('.index.current').text($indexThumb);

		},
		galleryFullsizeIndex: function(swiper){

			var $index	= swiper.activeIndex + 1;

			if( $index < 10 ){
				$index = '0' + $index;
			}

			$('.index-fullsize.current').text($index);

		},
		galleryLength: function(){
			
			var galleryLength = $('.wrapper-gallery').find('.block.list, .block.single').length;

			if( galleryLength < 10 ){
				galleryLength = '0' + galleryLength;
			}

			$('.index.last, .index-fullsize.last').text(galleryLength);

		},
		galleryVideoOverlay: function(){
			
			if( $('.swiper-slide-active').hasClass('video') ){
				$('#gallery-overlay').addClass('video');
			} else {
				$('#gallery-overlay').removeClass('video');
			}

		}
	};

	//if($("#slider-gallery").length) { GalleryHandler.init() }


	/** UPDATE 2017 **/

	/************************************
	 MAIN
	************************************/
	var mainHandler = {
		
		init: function(){
			var self = this;

			self.setPlayers();
		},
		setPlayers: function(){
			
			var i = 0;
			
			$('*[data-video-url]').each(function(){
				
				i++;
				
				var videoBlock = $(this);
				
				videoBlock.find('.video').attr('id', 'video-'+i);
				
				var videoURL = $(this).attr('data-video-url'),
					videoWidth = $(this).attr('data-video-width'),
					videoHeight = $(this).attr('data-video-height'),
					gridWidth = $('.video--wrapper').width(),
					gridHeight = ( videoHeight / videoWidth ) * gridWidth;
				
				if( $(this).hasClass('thumbnail') ){
					jwplayer('video-'+i).setup({
						file: videoURL,
						width: gridWidth,
						height: gridHeight,
						mute: true,
						autostart: true,
						repeat: true,
						stretching: 'fill'
					});
				} else {
					
					var start = false;
					
					if( i == 1 ){
						start = true
					}
					
					jwplayer('video-'+i).setup({
						file: videoURL,
						width: gridWidth,
						height: gridHeight,
						//mute: true,
						autostart: start,
						stretching: 'fill',
						repeat: true
					});
				}
				
			});
			
			$(window).on('resize', function(){
				
				$('.video--wrapper').each(function(){
					
					var videoID = $(this).find('.jwplayer').attr('id'),
						videoWidth = $(this).attr('data-video-width'),
						videoHeight = $(this).attr('data-video-height'),
						newWidth = $(this).width(),
						newHeight = ( videoHeight / videoWidth ) * newWidth;
						
					jwplayer(videoID).resize(newWidth,newHeight);
					
				});
				
			});
			
			
		}

	}
	mainHandler.init();

	/************************************
	 GALLERY GRID (NEW)
	************************************/
	var galleryListHandler = {
		
		init: function(){
			var self = this;

			self.setGalleries();
		},
		setGalleries: function(){

			var grid = $('.grid--galleries'),
				gridItem = $('.grid--item'),
				loader = $('<div class="loader"></div>');

			grid.parent().append(loader);

			grid.masonry({
				// options
				itemSelector: '.grid--item',
				gutter: 140,
				columnWidth: '.grid--item',
				percentPosition: true
				//columnWidth: 200
			});
			
			setTimeout(function(){
			
				grid.imagesLoaded().always( function(){
					
					grid.masonry('layout');

					loader.fadeOut(250);

					// Once evrything is loaded, hide loader & show thumbs
					var i=0;
					
					gridItem.each(function(){
						
						var item = $(this);
						
						setTimeout(function(){
							item.addClass('visible');
						}, 100*i);
						i++;

					});
				});
				
			}, 400);

		}

	}
	if($("#gallery--list").length) { galleryListHandler.init() }


	/************************************
	 GALLERY SINGLE (NEW)
	************************************/
	var galleryHandler = {
		
		init: function(){
			var self = this;
			
			self.setGallery();
		},
		setGallery: function(){
			
			var galleryFullscreen = new Swiper('.gallery--fullscreen', {
				speed: 400,
				//slidesPerView: 1,
				//autoHeight: true,
				//effect: 'fade',
				//autoHeight: true,
				//hashnav: true,
				//hashnavWatchState: true,
				//replaceState: true,
				//history: 'slide',
				preloadImages: false,
				lazyLoading: true,
				//lazyLoadingInPrevNext: true,
				//lazyLoadingInPrevNextAmount: 2,
				onLazyImageLoad: function(swiper){
					
				},
				simulateTouch: true,
				keyboardControl: true,
				onInit: function(swiper){
					//jwplayer('video-1').play();
				},
				onSlideChangeStart: function(swiper){
					
					var currentSlide = swiper.activeIndex + 1;
					
					if ( currentSlide < 10 ){
						currentSlide = '0' + currentSlide;
					}
						
					$('.gallery--index').text( currentSlide );
					
					var totalSlide = galleryFullscreen.slides.length;
					
					if ( totalSlide < 10 ){
						totalSlide = '0' + totalSlide;
					}
					
					$('.gallery--total').text( totalSlide );
					
					
				},
				onSlideChangeEnd: function(swiper){
					
					$('.jwplayer').each( function(){
						var jwplayerID = $(this).attr('id');
						//alert('player found:'+jwplayerID);
						jwplayer(jwplayerID).stop();
					});
					
					if ( $('.swiper-slide:eq('+swiper.activeIndex +') .jwplayer').length > 0 ){
						
						var videoID = $('.swiper-slide:eq('+swiper.activeIndex +') .jwplayer').attr('id');
						//console.log(videoID);
						
						
						
						jwplayer(videoID).play();
						
					} /*else {
						
						var otherPlayers = $('.swiper-slide .jwplayer');
						
						otherPlayers.each(function(){
							var videoID = $(this).attr('id');
							jwplayer(videoID).stop();
						});
						
					}*/
					
				}
			});
			
			
			var totalSlide = galleryFullscreen.slides.length;
					
			if ( totalSlide < 10 ){
				totalSlide = '0' + totalSlide;
			}
			
			$('.gallery--total').text( totalSlide );
			
			$('.swiper--nav-next').on('click', function(){
				galleryFullscreen.slideNext();
			});
			
			$('.swiper--nav-previous').on('click', function(){
				galleryFullscreen.slidePrev();
			});
			
			
			/*$('.jwplayer').each(function(){
				var videosID = $(this).attr('id');
				//jwplayer('video-1').play();
				
				
			});*/
			//jwplayer().on('ready', function(){
			//	jwplayer('video-2').stop();
			//});
			//jwplayer('video-2').play();
			/*if( $('.swiper-slide:eq(0)').hasClass('slide-video') ){
				
				var videoOne = $('.swiper-slide:eq(0)').find('.jwplayer').attr('id');
				alert(videoOne);
				jwplayer('video-1').play();
				//var otherPlayers = $('.jwplayer');
						
				//otherPlayers.each(function(){
					//var firstVideoID = $('.swiper-slide.slide-video:eq(0)').find('.jwplayer').attr('id');
					//console.log(firstVideoID);
					//jwplayer('video-1').play();
				//});
				
				//$(this)
				
			}*/
			
			

		},
		playVideos: function(){
			
			
			
		}

	}
	if($("#gallery").length) { galleryHandler.init() }


	/************************************
	 HOME
	************************************/
	var homeHandler = {
		
		init: function(){
			var self = this;

		}

	}
	if($("#home").length) { homeHandler.init() }
});