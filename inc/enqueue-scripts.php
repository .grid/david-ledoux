<?php
/* ENQUEUED SCRIPTS & STYLES */
function davledoux_scripts() {
	
	/* Register a custom version of jQuery from Google CDN, plus the jquery-migrate.js plugin for enhanced compatibility with jQuery 1.9/2.0 & older JS libraries */
	if( !is_admin() ){
		wp_deregister_script('jquery');
		wp_register_script('jquery', ("https://code.jquery.com/jquery-3.4.1.min.js"), array(), '3.4.1', false);
		wp_enqueue_script('jquery');
		wp_register_script('jquery-migrate', ("https://code.jquery.com/jquery-migrate-1.2.1.min.js"), array(), '1.2.1', false);
		wp_enqueue_script('jquery-migrate');
	}
			
	/* Enqueue our main theme's style.css file */
	wp_enqueue_style( 'davledoux-style', get_stylesheet_uri() );
	
	wp_register_style('vjs', 'https://vjs.zencdn.net/7.6.0/video-js.css','',false,'screen');
	wp_enqueue_style( 'vjs' );
	
	/* Register then Enqueue all scripts */
	/* Enqueue our main theme's scripts.js file */
	wp_register_script( 'davledoux_js', get_template_directory_uri() . '/js/scripts.min.js', array( 'jquery' ), '1.1', true );
	wp_enqueue_script( 'davledoux_js' );

	//wp_register_script( 'jwplayer', 'https://content.jwplatform.com/libraries/PKhrrvSP.js', array( 'jquery' ), '1.0', true );
	//wp_enqueue_script( 'jwplayer' );
	wp_register_script( 'vjs', 'https://vjs.zencdn.net/7.6.0/video.min.js', array( 'jquery' ), '7.6.0', true );
	wp_enqueue_script( 'vjs' );
	//wp_enqueue_style( 'swiper', get_template_directory_uri() . '/js/libs/swiper/css/swiper.css', array(), '3.2.7', 'screen' );
	//wp_register_script( 'swiper', get_template_directory_uri() . '/js/libs/swiper/js/swiper.min.js', array( 'jquery' ), '3.2.7', true );
	//wp_enqueue_script( 'swiper' );
	
}
add_action( 'wp_enqueue_scripts', 'davledoux_scripts' );
