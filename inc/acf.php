<?php
	
	// OPTIONS PAGES
	add_action('acf/init', 'acf_options_pages');

	function acf_options_pages() {
		
		if( function_exists('acf_add_options_page') ) {
			
			$option_page = acf_add_options_page(array(
				'page_title' 	=> 'Theme Options',
				'menu_title' 	=> 'Theme Options',
				'menu_slug' 	=> 'theme-options',
				'icon_url'		=> 'dashicons-admin-site',
				'position'		=> 4
			));
			
			/*acf_add_options_sub_page(array(
				'page_title' 	=> __('Options du thème', 'galbani'),
				'menu_title' 	=> __('Theme Options', 'galbani'),
				'parent_slug' 	=> 'theme-options',
				'slug'			=> 'galbani-options'
			));
			
			// Social Media
			acf_add_options_sub_page(array(
				'page_title' 	=> __('Réseaux Sociaux', 'galbani'),
				'menu_title' 	=> __('Réseaux Sociaux', 'galbani'),
				'parent_slug' 	=> 'theme-options',
				'slug'			=> 'galbani-social-media'
			));
			
			// 404 options
			acf_add_options_sub_page(array(
				'page_title' 	=> __('Page 404', 'galbani'),
				'menu_title' 	=> __('Page 404', 'galbani'),
				'parent_slug' 	=> 'theme-options',
				'slug'			=> 'galbani-404'
			));*/
			
		}
		
	}