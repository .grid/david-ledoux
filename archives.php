<?php get_header(); ?>
<?php if( have_posts() ) : ?>
<div class="wrapper-archives">
	<?php while( have_posts() ) : the_post(); ?>
	<div class="post">
		<div class="post-thumbnail">
			<?php echo get_the_post_thumbnail( $post->ID, 'thumb-blog' ); ?>
		</div>
		<div class="post-content">
			<?php the_content(); ?>
		</div>
		<span class="clearer"></span>
	</div>
	<?php endwhile; ?>
</div>
<?php endif; ?>
<?php get_footer(); ?>