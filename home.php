<?php get_header(); ?>

<main id="home">

	<section id="gallery--home">

		<?php $featuredCategories = get_field('home_featured_categories', 'options'); ?>

		<?php if( $featuredCategories ){ ?>
		<div class="grid--galleries">
					
			<?php

				foreach( $featuredCategories as $featuredCategory ){

				$mediaType = get_field('category_featured_type', 'work_category_' . $featuredCategory);
				
				if( $mediaType === 'video' ){

					// Get Category Featured Video
					$featuredCategoryVideoID = get_field('category_featured_video', 'work_category_' . $featuredCategory);
					$videoMeta = wp_get_attachment_metadata($featuredCategoryVideoID);
					
					$videoURL = wp_get_attachment_url( $featuredCategoryVideoID );
					$videoWidth = $videoMeta['width'];
					$videoHeight = $videoMeta['height'];

					$media = '';
					$media .= '<div class="video--wrapper home" data-video-url="' . $videoURL . '" data-video-width="' . $videoWidth . '" data-video-height="' . $videoHeight . '">';
					//$media .= '<div class="video"></div>';
					$media .= '<video class="video-js" preload="auto">';
					$media .= '<source src="' . $videoURL . '" type="video/mp4">';
					//<!--<source src="MY_VIDEO.webm" type="video/webm">-->
					$media .= '</video>';
					$media .= '</div>';

					$cssClass = 'video';

				} else if ($mediaType === 'photo' ) {
					
					$featuredCategoryPhotoID = get_field('category_featured_image', 'work_category_' . $featuredCategory);

					$media = '';
					$media .= wp_get_attachment_image( $featuredCategoryPhotoID, 'full' );

					$cssClass = 'image';

				}

				$featuredCategory = get_term( $featuredCategory, 'work_category' );
				$featuredCategoryName = $featuredCategory->name;
				$featuredCategoryLink = get_term_link( $featuredCategory );

			?>
			
			<div class="grid--item <?php echo $cssClass; ?>">
	
				<a href="<?php echo $featuredCategoryLink; ?>">

					<article class="grid--item-content">

						<?php echo $media; ?>

						<h2 class="grid--item-title"><?php echo $featuredCategoryName; ?></h2>

						<svg class="grid--item-icon" xmlns="http://www.w3.org/2000/svg" width="20" height="20" viewBox="0 0 20 20">
							<rect class="bar-horizontal" fill="#FFFFFF" x="0" y="9" width="20" height="1"/>
							<rect class="bar-vertical" fill="#FFFFFF" x="0" y="9" width="20" height="1"/>
						</svg>

					</article>

				</a>

			</div>

			<?php } ?>

		</div>
		<?php } ?>

	</section>

<?php get_footer(); ?>