# GRID Starter

What is it ?
============
GRID Starter is (another?) repository containing many useful files & resources dedicated to start any front-end application from-scratch.
This is a constantly-evolving-work-in-progress project... So use it wisely.

Below you will find some details & explanation about it:


Work Tree
============
The main working-tree for GRID Starter is located at `/_src/`
When compiling, all builded files are stored at `/_html/`


SCSS files
============
All SCSS files are organized within specific folders.

- `Assets` contains all mandatory files (like _grid.scss) or files that are not supposed to change too regularly (_breakpoints.scss or _reset.scss).
- `Styleguides` contains all layout-related styling like buttons, forms, headings & tables but also colors & fonts mixins.
- `Templates` contains all templates SCSS. We recommend using 1 SCSS for each specific template.
- `Modules`, if included, is used to add re-usable custom elements like breadcrumb, pagination etc.


HTML files
============
To avoid some serious headaches, HTML files are used just like in a WordPress Theme...
- header.html & footer.html are excluded from the Gulp build process, and are used as a template part in each template (using gulp-replace package).
- The `/tpl/` folder is also excluded from the build process. It is used to store any re-usable or generic HTML elements like modules.


JS files
============
JavaScripts files are located in the `/js/` folder. All files stored in this folder will be processed & minified to a scripts.min.js compiled output.

It is recommended to store every useful external library to `/js/libs/mylibrary/mylibray.js`, as they will also be processed and compiled into our main scripts.min.js file.