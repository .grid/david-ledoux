"use strict";
/******************************
* LINK GLOBALLY INSTALLED MODULES (CLI)
npm link gulp gulp-sass gulp-postcss css-mqpacker autoprefixer gulp-clean-css cssnano gulp-jshint gulp-concat gulp-minify gulp-nunjucks-render gulp-html-beautify gulp-imagemin gulp-rename gulp-plumber gulp-notify del browser-sync
******************************/

/******************************
* PROJECT SETTINGS
******************************/
var projectName		= 'David Ledoux | Photography & Mobing image',
	projectDesc		= '',
	projectLang		= 'fr',
	projectURL		= '',
	templateName	= 'david-ledoux',
	showCredits		= true,
	devpath			= './_src/',
	buildpath		= './_html/',
	wppath			= '../';


/******************************
* GULP & PLUGINS
******************************/
const gulp			= require('gulp');
const del			= require('del');

const sass			= require('gulp-sass');
const postcss		= require('gulp-postcss');
const mqpacker		= require('css-mqpacker');
const autoprefixer 	= require('autoprefixer');
const cleanCSS		= require('gulp-clean-css');
const cssnano		= require('cssnano');

const jshint		= require('gulp-jshint');
const concat		= require('gulp-concat');
const minify 		= require('gulp-minify');

const nunjucksRender = require('gulp-nunjucks-render');
const htmlBeautify 	= require('gulp-html-beautify');

const imagemin 		= require('gulp-imagemin');

const rename		= require('gulp-rename');

const plumber		= require('gulp-plumber');
const notify		= require('gulp-notify');

const browsersync = require('browser-sync').create();



// Plumber
var plumberOptions = {
	errorHandler: notify.onError({
		title: 'Gulp',
		message: 'Error: <%= error.message %>'
	})
};

// Clean assets
function clean() {
	return del([
		buildpath + '/**/*',
		'!'+ buildpath + 'json/**',
	]);
}


// BrowserSync
function browserSync(done) {
	browsersync.init({
		server: {
			baseDir: buildpath,
			directory: true
		},
		port: 1337
	});
	done();
}

// BrowserSync Reload
function browserSyncReload(done) {
	browsersync.reload();
	done();
}



  


/******************************
* WATCH FILES
******************************/
function watchFiles(){
	gulp.watch( devpath + '**/*.+(html|nunjucks|njk)', nunjucks );
	gulp.watch( devpath + 'css/**/*.scss', gulp.series(css, cssMin) );
	gulp.watch( devpath + 'js/**/*.js', gulp.series(jsLint, js) );
	gulp.watch( devpath + 'css/fonts/**', fonts);
	gulp.watch( devpath + 'images/**/*', images);
}


/******************************
██╗███╗   ███╗ █████╗  ██████╗ ███████╗███████╗
██║████╗ ████║██╔══██╗██╔════╝ ██╔════╝██╔════╝
██║██╔████╔██║███████║██║  ███╗█████╗  ███████╗
██║██║╚██╔╝██║██╔══██║██║   ██║██╔══╝  ╚════██║
██║██║ ╚═╝ ██║██║  ██║╚██████╔╝███████╗███████║
╚═╝╚═╝     ╚═╝╚═╝  ╚═╝ ╚═════╝ ╚══════╝╚══════╝
1.Images optimizations
******************************/
function images(){
	
	return gulp
		.src(devpath + 'images/**')
		.pipe(imagemin([
				imagemin.svgo({
				plugins: [ 
					{ removeUselessDefs: false },
					{ cleanupIDs: false} 
				]
				}),
				imagemin.gifsicle(),
				imagemin.jpegtran(),
				imagemin.optipng()
			])
		)
		.pipe(gulp.dest(buildpath + 'images/'))
		.pipe(gulp.dest(wppath + 'images/'))
		.pipe(browsersync.stream());
	
}

/******************************
██╗  ██╗████████╗███╗   ███╗██╗     
██║  ██║╚══██╔══╝████╗ ████║██║     
███████║   ██║   ██╔████╔██║██║     
██╔══██║   ██║   ██║╚██╔╝██║██║     
██║  ██║   ██║   ██║ ╚═╝ ██║███████╗
╚═╝  ╚═╝   ╚═╝   ╚═╝     ╚═╝╚══════╝
1. Build HTML using NUNJUCKS
******************************/
/******************************
* NUNJUCKS OPTIONS
******************************/
var nunjucksOptions = {
	path: [devpath + '/template-parts/'],
	data: {
		site_title: projectName,
		site_desc: projectDesc,
		site_lang: projectLang,
		site_template: templateName,
		site_url: projectURL,
		site_show_credit: showCredits
	}
};

// HTML
function nunjucks(){
	return gulp
		.src(devpath + '*.+(html|nunjucks|njk)')
		.pipe(plumber(plumberOptions))
		.pipe(nunjucksRender(nunjucksOptions))
		.pipe(htmlBeautify({
			"indent_size": 1,
			"indent_with_tabs": true,
			"max_preserve_newlines": 1,
			"jslint_happy": true,
		}))
		.pipe(gulp.dest(buildpath))
		.pipe(browsersync.stream());
}

/******************************
 ██████╗███████╗███████╗
██╔════╝██╔════╝██╔════╝
██║     ███████╗███████╗
██║     ╚════██║╚════██║
╚██████╗███████║███████║
 ╚═════╝╚══════╝╚══════╝
1. Compile SASS
2. Build Fonts
******************************/
	
var cssDefault = [
	autoprefixer({
		overrideBrowserslist: ['> 0.1%', 'Firefox > 20', 'IE 9', 'IE 10', 'IE 11'],
		cascade: false
	}),
	mqpacker({
		sort: true
	})
];

function css(){

	return gulp
		.src([ devpath + 'css/style.scss' ])
		.pipe(plumber(plumberOptions))
        .pipe(nunjucksRender(nunjucksOptions))
		.pipe(sass({
			errLogToConsole: true,
			//outputStyle: 'compact'
		}))
		.pipe(postcss(cssDefault))
		.pipe(cleanCSS({
			//mergeIntoShorthands: true,
			//minify: false,
			format: 'keep-breaks',
			level: 2
		}))
		.pipe(gulp.dest( buildpath ))
		.pipe(gulp.dest( wppath ))
		.pipe(browsersync.stream());

}

function cssMin(){

	var cssMin = [
        cssnano()
	];

	return gulp
		.src([ buildpath + 'style.css' ])
		.pipe(postcss(cssMin))
		.pipe(rename({suffix: '.min'}))
		.pipe(gulp.dest( buildpath ))
		.pipe(gulp.dest( wppath ));

}

function fonts(){
	return gulp
		.src(devpath + 'css/fonts/**')
		.pipe(gulp.dest( buildpath + 'css/fonts/' ))
		.pipe(gulp.dest( wppath + 'css/fonts/' ))
		.pipe(browsersync.stream());
}



/******************************
     ██╗███████╗
     ██║██╔════╝
     ██║███████╗
██   ██║╚════██║
╚█████╔╝███████║
 ╚════╝ ╚══════╝
1. Lint project JS main file
2. Concatenate & Minify all JS Files to scripts.js
3. Build Libs
******************************/
var scripts = [
	devpath + 'js/scripts.js',
	devpath + 'js/libs/imagesLoaded/imagesLoaded.js',
	devpath + 'js/libs/masonry/masonry.pkgd.js',
	devpath + 'js/libs/swiper/swiper.jquery.js'
];

function js(){
	
	return gulp
		.src(scripts)
		.pipe(plumber(plumberOptions))
		.pipe(concat('scripts.js'))
		.pipe(minify({
			ext:{
				min:'.min.js'
			}
		}))
		.pipe(gulp.dest( buildpath + 'js/' ))
		.pipe(gulp.dest( wppath + 'js/' ))
		.pipe(browsersync.stream());
}

function jsLint(){
	return gulp
		.src( devpath + 'js/scripts.js' )
		.pipe(plumber(plumberOptions))
		.pipe(jshint())
		.pipe(jshint.reporter('default'));
}

function jsLibs(){
	return gulp
		.src([ devpath + 'js/libs/'] )
		.pipe(gulp.dest( buildpath + 'js/' ))
		.pipe(browsersync.stream());
};

/******************************
  _ )  |  | _ _|  |     _ \    __| 
  _ \  |  |   |   |     |  | \__ \ 
 ___/ \__/  ___| ____| ___/  ____/ 

1.Watch Files For Changes
2.Build All
******************************/                    



// Tasks
const dev = gulp.series(clean, gulp.parallel(watchFiles, browserSync, gulp.series(css, cssMin, fonts), gulp.series(jsLint, js), nunjucks, images));
//const watch = gulp.parallel();


exports.dev = dev;
//exports.watch = watch;

exports.default = dev;