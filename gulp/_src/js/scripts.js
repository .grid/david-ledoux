$(document).ready(function() {
	
	/***********************************
	 INTRO
	***********************************/
	var video = jQuery('#intro-video')[0];

	/** INTRO **/
	jQuery('a.btn-enter').on('click', function(e){
		e.preventDefault();
		jQuery('#intro').fadeOut(300, function(){
			video.pause();
		});
	});


	/************************************
	 VIDEOS HANLDER
	************************************/
	var videosHandler = {
		
		init: function(){
			var self = this,
				$i = 0;
				
			$('.video--wrapper').each(function(){
				
				$i++;

				$(this).find('.video-js').attr('id', 'video-'+$i);
				
				self.createPlayer('video-'+$i);
				
				self.resizePlayer('video-'+$i);

			});
			
			$(window).on('resize', function(){

				$('.video--wrapper').each(function(){
					
					var $playerID = $(this).find('.video-js').attr('id');

					self.resizePlayer($playerID);

				});
				

			});

		},
		createPlayer: function($playerID){

			var $videoURL = $('#'+$playerID).parent().attr('data-video-url'),
				$videoRealWidth = $('#'+$playerID).attr('width'),
				$videoRealHeight = $('#'+$playerID).attr('height'),
				$videoAudio = $('#'+$playerID).parent().attr('class').match(/thumbnail|home/) ? true : false,
				$videoControls = $('#'+$playerID).parent().attr('class').match(/thumbnail|home/) ? false : true,
				$params = {
					//file: $videoURL,
					//width: $videoRealWidth,
					//height: $videoRealHeight,
					autoplay: true,
					muted: true,
					loop: true,
					//fluid: true,
					controls: $videoControls
				};
				
			var player = videojs($playerID, $params);

		},
		resizePlayer: function($playerID){

			var $videoRealWidth = $('#'+$playerID).parent().attr('data-video-width'),
				$videoRealHeight = $('#'+$playerID).parent().attr('data-video-height'),
				$wrapperWidth = parseInt( $('#'+$playerID).parents('.video--wrapper').width() ),
				$wrapperHeight = parseInt( ( $videoRealHeight / $videoRealWidth ) * $wrapperWidth ),
				$playerWidth,
				$playerHeight;

			//console.log($wrapperWidth, $wrapperHeight);

			if( $('#'+$playerID).parent().hasClass('home') ){
				
				if (window.matchMedia("(min-width: 768px)").matches) {
					
					// Set video from Image height
					$wrapperHeight = $('#gallery--home .grid--item.image img').height();
					$wrapperWidth = ($wrapperHeight*$videoRealWidth) / $videoRealHeight;

					/*$('#gallery--home .grid--item.video .grid--item-content').css({
						'width': $wrapperWidth
					});*/

				} else {
					
					$wrapperWidth = parseInt( $('#'+$playerID).parent().width() );
					$wrapperHeight = parseInt( ( $videoRealHeight / $videoRealWidth ) * $wrapperWidth );

					/*$('#gallery--home .grid--item.video .grid--item-content').css({
						'width': '100%'
					});*/

				}

				// Thumbs
				videojs($playerID).width($wrapperWidth);
				videojs($playerID).height($wrapperHeight);

			} else if( $('#'+$playerID).parent().hasClass('thumbnail') ){
				
				// Thumbs
				videojs($playerID).width($wrapperWidth);
				videojs($playerID).height($wrapperHeight);

			} else {
				
				// Videos
				// In landscape, set Player from Height
				if (window.matchMedia("(orientation: landscape)").matches) {
					$wrapperHeight = parseInt( $('#'+$playerID).parent().height() );
					$wrapperWidth = ($videoRealWidth*$wrapperHeight)/$videoRealHeight;
				} else {
					$wrapperHeight = ($videoRealHeight*$wrapperWidth)/$videoRealWidth;
				}

				videojs($playerID).width($wrapperWidth);
				videojs($playerID).height($wrapperHeight);

			}

		}

	}
	videosHandler.init();

	/************************************
	 GALLERY LIST
	************************************/
	var galleryListHandler = {
		
		init: function(){
			
			var self = this;

			self.masonryGallery();

		},
		masonryGallery: function($horizontal){
			
			if ($horizontal === undefined) {
				$horizontal = false;
			}

			var $grid = $('.grid--galleries'),
				$gridItem = $('.grid--item'),
				$loader = $('<div class="loader"></div>');

			$grid.parent().append($loader);

			$grid.masonry({
				columnWidth: '.grid--sizer',
				itemSelector: '.grid--item',
				percentPosition: true,
				horizontalOrder: $horizontal
			});

			setTimeout(function(){
				$grid.masonry('layout');
			}, 1000);
			
			$grid.imagesLoaded().always( function(){
				
				$grid.masonry('layout');

				$loader.fadeOut(250);

				// Once evrything is loaded, hide loader & show thumbs
				var $i=0;
				
				$gridItem.each(function(){
					
					var $item = $(this);

					setTimeout(function(){
						$item.addClass('visible');
					}, 100*$i);
					$i++;

				});

			});

		}

	}
	if($("#gallery--list").length) { galleryListHandler.init() }


	/************************************
	 GALLERY SINGLE (NEW)
	************************************/
	var galleryHandler = {
		
		init: function(){
			var self = this;
			
			self.setGallery();
		},
		setGallery: function(){
			
			var galleryFullscreen = new Swiper('.gallery--fullscreen', {
				speed: 400,
				//slidesPerView: 1,
				//autoHeight: true,
				//effect: 'fade',
				//autoHeight: true,
				//hashNav: true,
				//hashNavWatchState: true,
				//replaceState: true,
				//history: 'slides',
				preloadImages: false,
				lazyLoading: true,
				onLazyImageLoad: function(swiper){
					
				},
				simulateTouch: true,
				keyboardControl: true,
				onInit: function(){
					
					var $slide = $('.swiper-slide.video'),
						$i = 0;

					$slide.each(function(){
						$i++;
						if( $i === 1 && $(this).hasClass('swiper-slide-active') ){
							videojs('video-'+$i).muted(false);
						} else {
							videojs('video-'+$i).muted(true);
						}

					});

				},
				onSlideChangeStart: function(swiper){
					
					var currentSlide = swiper.activeIndex + 1;
					
					if ( currentSlide < 10 ){
						currentSlide = '0' + currentSlide;
					}
						
					$('.gallery--index').text( currentSlide );
					
					var totalSlide = swiper.slides.length;
					
					if ( totalSlide < 10 ){
						totalSlide = '0' + totalSlide;
					}
					
					$('.gallery--total').text( totalSlide );
					
					
				},
				onSlideChangeEnd: function(swiper){
					
					$('.swiper-slide.video').each(function(){
						var $playerID = $(this).find('.video-js').attr('id');
						videojs($playerID).pause();
					});

					if ( $('.swiper-slide:eq('+swiper.activeIndex +')').hasClass('video') ){
						
						var $playerID = $('.swiper-slide:eq('+swiper.activeIndex +').video').find('.video-js').attr('id');
						videojs($playerID).play();
						videojs($playerID).muted(false);
						
					}
					
				}

			});
			
			
			var totalSlide = galleryFullscreen.slides.length;
					
			if ( totalSlide < 10 ){
				totalSlide = '0' + totalSlide;
			}
			
			$('.gallery--total').text( totalSlide );
			
			$('.swiper--nav-next').on('click', function(){
				galleryFullscreen.slideNext();
			});
			
			$('.swiper--nav-previous').on('click', function(){
				galleryFullscreen.slidePrev();
			});

		}

	}
	if($("#gallery").length) { galleryHandler.init() }


	/************************************
	 HOME
	************************************/
	var homeHandler = {
		
		init: function(){
			var self = this;

			//this.setHomeGallery();

		},
		setHomeGallery: function(){
			
			// $Horizontal layout
			galleryListHandler.masonryGallery(true);

		}

	}
	if($("#home").length) { homeHandler.init() }
});