<?php get_header(); ?>
<?php if( have_posts() ) : ?>
<?php while( have_posts() ) : the_post(); ?>
<div class="wrapper-post">
	<?php if( get_the_title () ){ ?>
	<!--<h1 class="title"><?php echo get_the_title(); ?></h1>-->
	<?php } ?>
	<div class="post">
		<div class="post-thumbnail">
			<?php echo get_the_post_thumbnail( $post->ID, 'thumb-blog' ); ?>
		</div>
		<div class="content-wrapper">
			<div class="post-content">
				<?php the_content(); ?>
				<p class="post-date"><?php echo get_the_date('d/m/Y'); ?></p>
			</div>
			<aside>
				<?php get_template_part('tpl/sharers'); ?>
			</aside>
		</div>
		<span class="clearer"></span>
	</div>
</div>
<?php endwhile; ?>
<?php endif; ?>
<?php get_footer(); ?>