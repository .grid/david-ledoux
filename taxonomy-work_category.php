<?php get_header(); ?>

<main id="galleries">

	<section id="gallery--list">

		<?php if( have_posts() ) { ?>
		
			<div class="grid--galleries">

				<div class="grid--sizer"></div>
			
				<?php while( have_posts() ) : the_post(); ?>
				
				<?php
					$gallery = get_field('acf_gallery_pictures');
					$galleryFeaturedType = $gallery[0]['acf_gallery_picture_type'];
				?>

				<div class="grid--item <?php echo $galleryFeaturedType; ?>">
	
					<a href="<?php echo get_permalink(); ?>">
					<?php
						if( $galleryFeaturedType === 'video' ){
							get_template_part( 'tpl/galleries/thumb-video' );
						} else {
							get_template_part( 'tpl/galleries/thumb-image' );
						}
						?>
					</a>

				</div>

				<?php endwhile; ?>
	
			</div>
		
		<?php } ?>

	</section>

</main>

<?php get_footer(); ?>